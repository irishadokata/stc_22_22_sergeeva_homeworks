public class Main {
    public static void main(String[] args) {
        Square square = new Square(5, 6, 10);
        Rectangle rectangle = new Rectangle(6, 7, 10, 13);
        Сircle сircle = new Сircle(8, 7, 15);
        Ellipse ellipse = new Ellipse(9, 7, 5, 10);

        System.out.println("Площадь квадрата:  " + square.areaSquare());
        System.out.println("Периметр квадрата:  " + square.perimeterSquare());
        System.out.println("Площадь прямоугольника:  " + rectangle.areaRectangle());
        System.out.println("Периметр прямоугольника:  " + rectangle.perimeterRectangle());
        System.out.println("Площадь круга:  " + сircle.areaСircle());
        System.out.println("Периметр круга:  " + сircle.perimeterСircle());
        System.out.println("Площадь эллипса:  " + ellipse.areaEllipse());
        System.out.println("Периметр эллипса:  " + ellipse.perimeterEllipse());
        square.movingFigure(45, 50);
    }
}
