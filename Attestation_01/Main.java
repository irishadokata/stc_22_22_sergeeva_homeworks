import java.io.IOException;

public class Main {
    public static void main(String[] args) throws IOException {
        ProductsRepository productsRepostory = new ProductsRepositoryFileBasedImpl("file.txt");
        System.out.println("Продукт по ID найден:  \n" + productsRepostory.findProductById(6));
        System.out.println("Результат поиска по образцу: \n" + productsRepostory.findByText("Гр"));
    }
}

