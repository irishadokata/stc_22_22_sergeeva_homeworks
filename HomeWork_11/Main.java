public class Main {
    public static void main(String[] args) {
        CarsRepository carsRepositoryFileBased = new CarsRepositoryFileBasedImpl("listOfCars.txt");
        System.out.println("Количество машин в указанном ценовом диапазоне:  \n" + carsRepositoryFileBased.findCarPriceRange(600, 820));
        System.out.println("Цвет самой дешевой машины:   \n" + carsRepositoryFileBased.findColorByCarCost());
        System.out.println("Средняя стоимость всех машин:  \n" + carsRepositoryFileBased.findAverageCostCar());
    }
}

