package ru.inno.ec.services;

import ru.inno.ec.models.Lesson;
import java.util.List;

public interface LessonsService {
    List<Lesson> getAllLessons();

    void addLessons(Lesson lesson);

    Lesson getLesson(Long id);

    void addLesson(Lesson lesson);

    void updateLessons(Long lessonId, Lesson lesson);

    void deleteCourse(Long lessonId);
}


