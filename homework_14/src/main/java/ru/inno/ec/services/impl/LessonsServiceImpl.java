package ru.inno.ec.services.impl;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.inno.ec.models.Lesson;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.repositories.LessonsRepository;
import ru.inno.ec.services.LessonsService;
import java.util.List;

@Service
@RequiredArgsConstructor
@Data
public class LessonsServiceImpl implements LessonsService {
    private final LessonsRepository lessonsRepository;
    private final CoursesRepository coursesRepository;

    @Override
    public List<Lesson> getAllLessons() {
        return lessonsRepository.findAllByStateNot(Lesson.State.DELETED);
    }

    @Override
    public void addLessons(Lesson lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .state(Lesson.State.NOT_CONFIRMED)
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public Lesson getLesson(Long lessonId) {
        return lessonsRepository.findById(lessonId).orElseThrow();
    }

    @Override
    public void addLesson(Lesson lesson) {
        Lesson newLesson = Lesson.builder()
                .name(lesson.getName())
                .summary(lesson.getSummary())
                .state(Lesson.State.NOT_CONFIRMED)
                .build();
        lessonsRepository.save(newLesson);
    }

    @Override
    public void updateLessons(Long lessonId, Lesson updateData) {
        Lesson lessonForUpdate = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForUpdate.setName(updateData.getName());
        lessonForUpdate.setSummary(updateData.getSummary());
        lessonsRepository.save(lessonForUpdate);
    }

    @Override
    public void deleteCourse(Long lessonId) {
        Lesson lessonForDelete = lessonsRepository.findById(lessonId).orElseThrow();
        lessonForDelete.setState(Lesson.State.DELETED);
        lessonsRepository.save(lessonForDelete);
    }
}
