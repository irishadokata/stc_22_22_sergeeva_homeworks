package ru.inno.ec.services.impl;


import org.springframework.stereotype.Service;
import ru.inno.ec.models.Course;
import ru.inno.ec.models.User;
import ru.inno.ec.repositories.CoursesRepository;
import ru.inno.ec.repositories.UsersRepository;
import ru.inno.ec.services.CoursesService;

import java.util.List;

@Service
public class CoursesServiceImpl implements CoursesService {

    private final CoursesRepository coursesRepository;
    private final UsersRepository usersRepository;

    public CoursesServiceImpl(CoursesRepository coursesRepository, UsersRepository usersRepository) {
        this.coursesRepository = coursesRepository;
        this.usersRepository = usersRepository;
    }

    @Override
    public void addStudentToCourse(Long courseId, Long studentId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        User student = usersRepository.findById(studentId).orElseThrow();
        student.getCourses().add(course);
        usersRepository.save(student);
    }

    @Override
    public Course getCourse(Long courseId) {
        return coursesRepository.findById(courseId).orElseThrow();
    }

    @Override
    public List<User> getNotInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesNotContains(course);
    }

    @Override
    public List<User> getInCourseStudents(Long courseId) {
        Course course = coursesRepository.findById(courseId).orElseThrow();
        return usersRepository.findAllByCoursesContains(course);
    }

    @Override
    public List<Course> getAllCourses() {
        return coursesRepository.findAllByStateNot(Course.State.DELETED);
    }

    @Override
    public void addCourse(Course course) {
        Course newCourse = Course.builder()
                .title(course.getTitle())
                .description(course.getDescription())
                .state(Course.State.NOT_CONFIRMED)
                .build();
        coursesRepository.save(newCourse);
    }

    @Override
    public void updateCourse(Long courseId, Course updateData) {
        Course courseForUpdate = coursesRepository.findById(courseId).orElseThrow();
        courseForUpdate.setTitle(updateData.getTitle());
        courseForUpdate.setDescription(updateData.getDescription());
        coursesRepository.save(courseForUpdate);
    }

    @Override
    public void deleteCourse(Long courseId) {
        Course courseForDelete = coursesRepository.findById(courseId).orElseThrow();
        courseForDelete.setState(Course.State.DELETED);
        coursesRepository.save(courseForDelete);
    }
}
